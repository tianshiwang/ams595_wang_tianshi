%(2)set a fixed significant figures to calculate pi and count steps...
%Main Idea is that using while loop to calculate pi value which satisfy...
%required precision. I calculate interval of last 30 pi values and
%then ask the interval less than certain value that I set to represent ...
%precision.

totalnum =1;%total number of points
error = 1;% difference between two calculated pi vlaues which are next to each other
r=1;%radious of the circle
numin = 0;%total number of points in the circle
outcome = [];% Array that store all the calculated pi values

tic
% while loop to calculate PI with 5 significant figures
while error >= 0.001
    
    %generate random value
    x = rand;
    y = rand;
    
    %calculate pi value
    if x^2 + y^2 <= r^2
        numin = numin + 1;
    end
    outcome(totalnum) = (numin / totalnum) *4;
    %
    if totalnum >1000
        error = max(outcome(totalnum -50 :totalnum))-min(outcome(totalnum -50 :totalnum));
    else
        error = 1;
    end
    
    % step counter also total number of points 
    totalnum = totalnum + 1;
end
toc
%total steps
step = totalnum - 1;

% plot outcome of calculated pi
i = 1:(totalnum-1);
plot(i, outcome);
hold on

% line of true pi
plot([1 totalnum],[1 1]*pi, 'LineWidth', 1);
hold off
    
