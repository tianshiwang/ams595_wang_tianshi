%example input matrix
in =   [10,-7,0,3,3,3,10
        -3,2,6,2,2,2,2
        5,-1,5,4,5,6,2
        1,2,3,4,5,6,2
        2,3,4,5,6,7,2
        1,3,5,7,9,5,2
        1,2,3,4,5,6,7];
 %example b vector   
 c = [1
      2
      3
      4
      5
      6
      7];
 %those are call functions,you can enable them to test functions
 %[x,y] = matrix_test(in);
 B = Re_echelon(in);
 C = Re_canonical(B);
 D = Matrix_sol(in,c);
 E = element_M(in);
 F = inverse_M(in);
 [P,U,L] = LU_de(in);
 
%PART1 (e)
%square matrix case
 i = 1;
for m = 20:150
    n =m;
    for row = 1:m     
        b(row) = round(1+rand*10);  %create random b
         for col = 1:n
             A(row , col) = round(1+rand*10);  %create random matrix
         end
    end 
    M_size(i) = m*m;    %size of the matrix
    bt = transpose(b);   % transpose of b vector
    tic                  % count time
    outcome = Matrix_sol(A,bt);
    toc
    time(i) = toc;
    i = i+1;
end
plot(M_size,time);      % plot graph
%fix rows, various columns case
 j = 1;
 m = 20;
for n =20:150
    for row = 1:m     
        b1(row) = round(1+rand*10);  %create random b
         for col = 1:n
             A1(row , col) = round(1+rand*10);  %create random matrix
         end
    end 
    M_size1(j) = m*n;    %size
    bt = transpose(b1);  %transpose
    tic                  % count time
    outcome = Matrix_sol(A1,bt);
    toc
    time1(j) = toc;
    j = j+1;
end
figure
plot(M_size1,time1);
% fix column, various rows case
 k = 1;
 n =20;
for m =20:150
    for row = 1:m     
        b2(row) = round(1+rand*10);  %create random b
         for col = 1:n
             A2(row , col) = round(1+rand*10);  %create random matrix
         end
    end 
    M_size2(k) = m*n;    %size of matrix
    bt = transpose(b2);  
    tic
    outcome = Matrix_sol(A2,bt);
    toc
    time2(k) = toc;
    k = k+1;
end
figure
plot(M_size2,time2);


 %Part 2
 %(A) find elementary matrix
 function outcome = element_M(A)
 [Om,On] = size(A);
 iden = 1;
for row = 1:Om     % expand matrix with identity matrix
    A(row, On+iden) = 1;
    iden = iden+1;
end 
[m,n] = size(A);
 ech = Re_echelon(A);   %transform to echelon
 can = Re_canonical(ech);  % trasform to row-canonical
 outcome = can(:,On+1:n);  % elementary matrix
 end 
 
 %?B) find inverse matrix 
 function outcome = inverse_M(input)
 if isfile('input')  %test input is text or matrix
     A = dlmread(input);
 else
     A = input;
 end    
 [m,n] = size(A);
 % matrix is not square
 if m ~= n 
     outcome = [];
     disp('matrix is singular');
 else                   % matrix is square
     ech = Re_echelon(A);    % change to echelon
     can = Re_canonical(ech);  % change to row-canonical
     if can == eye(m)           %chack canonical is identity
         outcome = element_M(A);
         disp(outcome);
     else
         outcome = [];
         disp('matrix is singular');
     end
 end
 end
 
 %(C) LU de composition
 function [P,U,L] = LU_de(A)
M=A; 
%expand matrix
[Om,On] = size(A);
iden = 1;
for row = 1:Om
    A(row, On+iden) = 1;
    iden = iden+1;
end    

AP = eye(Om);    % set P,M equeal to identity
AM = eye(Om);
[m,n]=size(A);

 pivot_index = 1;
 for col = 1:n
     for row = pivot_index:m
         if A(row,col) ~= 0
              exch = A(row,:);
              A(row,:) = A(pivot_index,:);
              A(pivot_index,:) = exch;
              %create P matrix
              exch1 = AP(row,:);
              AP(row,:) = AP(pivot_index,:);
              AP(pivot_index,:) = exch1;
           
              for row_zero = pivot_index+1:m
                  if A(row_zero , col) ~= 0
                      coef = A(row_zero , col)/A(pivot_index,col);
                      A(row_zero , :) = A(row_zero , :) - coef*A(pivot_index,:);
                      %create M matrix
                      AM(row_zero , :) = AM(row_zero , :) - coef.*AM(pivot_index,:);
                  end
              end
              
              pivot_index = pivot_index + 1;
              break
         end
     end 
     if pivot_index-1 == m
         break
     end    
     
 end
AU = AM * AP * M;   %U matrix
AL= inv(AM);         % L matrix
U =AU;
P = AP;
L =AL;
 end 
 %Part1
 %(d) solve AX = b
 function outcome = Matrix_sol(A,b)
 [m,n] = size(A);
 [vm,vn] = size(b);
 %test if b is valid
 if vm ~= m || vn ~= 1
     outcome = [];
     disp('input valid vector');
     
 else %valid
     M = A;
     M(:,n+1) = b;   %expand matrix
     [Mm,Mn] = size(M);
     echelon_form = Re_echelon(M);  %convert input matrix into echelon form
     canonial_form = Re_canonical(echelon_form);
     % find no solution case    
     if rank(canonial_form) ~= rank (A)
         disp('no solution');
         outcome = [];
     elseif rank(A) < n      % infinite solution case
        disp('infinite solutions');
        outcome = [];
     elseif rank(A) == n     % unique solution case
         outcome = canonial_form(:,Mn);
         disp(outcome);
     end    
     
 end
 end
 %(c)trasform echelon to row-canonical form
 function outcome = Re_canonical(A)
 test_value = Echelontest(A);  %check if the input matrix is echelon form
 if test_value == 0 % input is not echelon form
     disp('error, you need to input echelon form');
     outcome = [];
 else               % input is echelon form
     [m,n] = size(A);
     echelon_form = Re_echelon(A);
     for row = m:-1:1
         for col = 1:n
             % find pivot
             if echelon_form(row, col) ~= 0 
                 % make pivot = 1
                 echelon_form(row,:) = echelon_form(row,:).*(1/echelon_form(row, col));
                 % make all entries above pivot = 0
                 for row_zero = row-1 : -1 : 1
                     if echelon_form(row_zero, col) ~= 0
                         echelon_form(row_zero,:) = echelon_form(row_zero,:)-echelon_form(row,:).*echelon_form(row_zero, col);
                     end
                 end
                 
                 break
             end
         end   
     end  
     outcome = fix(10^10*echelon_form)/10^10;
 end            
                    
 end                                                  
%(b) transform to echelon form
 function outcome = Re_echelon(A)
 [m,n] = size(A);  % dtect size of input matrix
 pi_dex = 1;       % pivot count
 %swap row 
 for col = 1:n
     for row = pi_dex:m
         if A(row,col) ~= 0
              exch = A(row,:);
              A(row,:) = A(pi_dex,:);
              A(pi_dex,:) = exch;
              % sutitution all entries under the pivot
              for row_zero = pi_dex+1:m
                  if A(row_zero , col) ~= 0
                      coef = A(row_zero , col)./A(pi_dex,col);
                      A(row_zero , :) = A(row_zero , :) - coef.*(A(pi_dex,:));
                  end
              end
              
              pi_dex = pi_dex + 1;
              break
         end
     end 
     if pi_dex-1 == m  %it is last row
         break
     end    
     
 end
 outcome = fix(10^10*A)/10^10;   %make outcome equal to real zero
 
 
 end
%(a) function to check both echelon and row-canonical
 function [x,y] = matrix_test(A)
 x = Echelontest(A);    % echelon check
 y = row_canonical(A);  % row-canonical check
 end
 % funtion check row-canonical form
function leagle = row_canonical(A)
 %find size of input matrix
[m,n] = size(A); 
% find zero-row
leagle = 1; 
nonzero_row = 0;
find_zero = 0;
i = 1;
for row = 1:m
    nonzero_row = 0;
    for col = 1:n
        if (A(row,col) ~= 0)
            nonzero_row = 1;
            break;
        end 
    end   
    %nonzero_row at the bottom
    if(nonzero_row == 0)
       find_zero = 1;
    end   
    if(nonzero_row ==1 && find_zero==1)
       leagle = 0;
       break
    end 
    %all pivots are to the right of above one
    if nonzero_row == 0 
        pivot_index(i) = 0;
        
    elseif nonzero_row == 1
        % if pivot is 1
        pivot_index(i)=find(A(row,:),1,'first');
        if A(row, pivot_index(i)) ~=1
            leagle = 0;
            break
        end
        %if only 1 nonzero entry
        nonzero_index=find(A(row,:));
        nonzero_size = size(nonzero_index);   
        if  nonzero_size(2) >1
            leagle = 0;
            break
        end    
        if (row > 1)
            if pivot_index(i) == pivot_index(i-1) % pivot is right under last pivot               
                leagle = 0;
                break
            elseif pivot_index(i) < max(pivot_index)  %pivot is not strickly to the right
                leagle = 0;
                break
                
            end    
        end   
    end
    
     
    i=i+1;
end
end

 % if leagle=0, matrix is not echelon form, if leagle=1, matrix is echelon
 % form.
 function leagle = Echelontest(A)
 
%find size of input matrix
[m,n] = size(A); 
% find zero-row 
leagle = 1; 
nonzero_row = 0;
find_zero = 0;
i = 1;
for row = 1:m
    nonzero_row = 0;
    for col = 1:n
        if (A(row,col) ~= 0) %find non-zero row
            nonzero_row = 1;  
            break;
        end 
    end    
    if(nonzero_row == 0)
       find_zero = 1;
    end   
    if(nonzero_row ==1 && find_zero==1)  %check if zero-row all at bottom
       leagle = 0;
       break
    end 
    % check if every pivots are stricktly right of last one
    if nonzero_row == 0 
        pivot_index(i) = 0;
        
    elseif nonzero_row == 1
        pivot_index(i)=find(A(row,:),1,'first');
        
        if (row > 1)
            if pivot_index(i) == pivot_index(i-1) % pivot is right under last pivot               
                leagle = 0;
                break
            elseif pivot_index(i) < max(pivot_index) %pivot is not strickly to the right
                leagle = 0;
                break
            end    
        end   
    end
    
     
    i=i+1;
end
end


